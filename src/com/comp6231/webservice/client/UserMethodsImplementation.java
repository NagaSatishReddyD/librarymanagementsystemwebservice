package com.comp6231.webservice.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.NotBoundException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.comp6231.webservice.constants.LibraryManagementConstants;
import com.comp6231.webservice.serviceinterface.LibraryManagementInterface;


/**
 * UserMethodsImplementaion is used to implement the client operations which send the data to the respective servers.
 * @author Naga Satish Reddy
 */
public class UserMethodsImplementation {

	static LibraryManagementInterface libraryManagementInterface;
	URL serverUrl;
	QName compQName;
	/**
	 * getUser method return the user Id by verifying the validations
	 * @return
	 * @throws IOException 
	 */
	public String getUser() throws IOException {
		String userId = null;
		boolean userValidation = true;
		do {
			BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
			String givenInput = reader.readLine().toUpperCase().trim();
			String universityCode = givenInput.substring(0, 3);
			boolean universityCodeVerify = universityCode.equals(LibraryManagementConstants.CONCORDIA_CODE) || 
					universityCode.equals(LibraryManagementConstants.MCGILL_CODE) || 
					universityCode.equals(LibraryManagementConstants.MONTREAL_CODE);
			String userCode =  givenInput.substring(3, 4);
			boolean userCodeVerify = userCode.equals(LibraryManagementConstants.USER_CODE) || 
					userCode.equals(LibraryManagementConstants.MANAGER_CODE);
			try {
				Integer.parseInt(givenInput.substring(5));
				if(universityCodeVerify && userCodeVerify && givenInput.length() == 8) {
					userValidation = false;
					userId = givenInput;
				}else {
					System.out.println("Please insert valid ID : ");
				}
			}catch(NumberFormatException exception) {
				System.out.println("Please insert valid ID : ");
			}
		}while(userValidation);
		return userId.toUpperCase();
	}

	/**
	 * showOperations method is used to find the user type and show the respective operations.
	 * @param userId
	 * @throws NotBoundException
	 * @throws IOException
	 */
	public void showOperations(String userId) throws NotBoundException, IOException {
		if(userId.subSequence(3, 4).equals(LibraryManagementConstants.MANAGER_CODE)) {
			showManagerOperations(userId);
		}else {
			showUserOperations(userId);
		}
	}

	/**
	 * showUserOpeartions is used to show operations available for user
	 * @param userId
	 * @throws IOException 
	 * @throws NotBoundException 
	 */
	private void showUserOperations(String userId) throws IOException, NotBoundException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			System.out.println("Please select from below actions");
			System.out.println("1. Borrow book");
			System.out.println("2. Return book");
			System.out.println("3. Find book");
			System.out.println("4. Exchange Book");
			switch(Integer.parseInt(reader.readLine().trim())) {
			case 1: borrowBook(userId, reader);
			break;
			case 2: returnBook(userId, reader);
			break;
			case 3: findBook(userId, reader);
			break;
			case 4: exchangeItem(userId, reader);
			break;
			}
		}
	}

	/**
	 * exchangeItem method is used to exchange the books for the user
	 * @param userId
	 * @param reader
	 * @throws IOException 
	 */
	private void exchangeItem(String userId, BufferedReader reader) throws IOException {
		System.out.println("Enter the New ItemId : ");
		String newItemId = verifyItemId(reader.readLine().trim(), reader);
		System.out.println("Enter the Old ItemId :");
		String oldItemId = verifyItemId(reader.readLine(), reader);
		this.insertUserLog(userId, "Exchange Item New Item ID : "+newItemId+" Old Item ID : "+oldItemId);
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		String response = libraryInterface.exchangeItem(userId, newItemId, oldItemId);
		this.insertUserLog(userId, "Exchange Item New Item ID : "+newItemId+" Old Item ID : "+oldItemId +" Response : "+response);
		System.out.println("Exchange Item New Item ID : "+newItemId+" Old Item ID : "+oldItemId +" Response : "+response);
	}


	/**
	 * returnBook handles the request of user to return his book.
	 * @param userId
	 * @param reader
	 * @throws IOException
	 * @throws NotBoundException
	 */
	private void returnBook(String userId, BufferedReader reader) throws IOException, NotBoundException {
		System.out.println("Please enter itemID");
		String itemId = verifyItemId(reader.readLine().trim(), reader);
		this.insertUserLog(userId, userId +" Return itemID : "+itemId);
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		String response = libraryInterface.returnItem(userId, itemId);
		this.insertUserLog(userId, userId +" Return Response itemID : "+itemId+ " "+ response);
		System.out.println(response);
	}

	/**
	 * findBook is used to handles the user request to find the book.
	 * @param userId
	 * @param reader
	 * @throws IOException
	 * @throws NotBoundException
	 */
	private void findBook(String userId, BufferedReader reader) throws IOException, NotBoundException {
		System.out.println("Please enter itemName");
		String itemName = reader.readLine().trim();
		this.insertUserLog(userId, "Find Book: "+ itemName);
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		String response = libraryInterface.findItem(userId, itemName, false);
		this.insertUserLog(userId, "Server Response Find Book: "+ response);
		System.out.println(response);
	}

	/**
	 * borrowBook method handles the request from the user to borrow request
	 * @param userId
	 * @param reader 
	 * @throws IOException
	 * @throws NotBoundException
	 */
	private void borrowBook(String userId, BufferedReader reader) throws IOException, NotBoundException {
		System.out.println("Please enter item ID : ");
		String itemId = verifyItemId(reader.readLine().trim(), reader);
		insertUserLog(userId, userId+" Requested Borrow Book "+itemId);
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		String response = libraryInterface.borrowItem(userId, itemId);
		insertUserLog(userId, "Server Response: "+ response);
		if(response.equals(LibraryManagementConstants.WAITING_LIST_MESSAGE)) {
			System.out.println(response);
			String option = reader.readLine();
			insertUserLog(userId, userId+" Interested In WaitingList: "+option);
			if(option.substring(0, 1).toUpperCase().equals("Y")) {
				insertUserLog(userId, userId+" Interested In WaitingList "+itemId);
				response = libraryInterface.addToWaitingList(userId, itemId);
				insertUserLog(userId, userId+" Server Response Interested In WaitingList "+itemId);
			}else {
				response = "Not added to waiting list";
			}
		}
		System.out.println("Server Response : "+response);
	}

	/**
	 * showManagerOperations is used to show operations available for manager
	 * @param userId
	 * @throws NotBoundException 
	 * @throws IOException 
	 */
	private void showManagerOperations(String userId) throws NotBoundException, IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			System.out.println("Please select from below actions");
			System.out.println("1. Add Items");
			System.out.println("2. Remove Items");
			System.out.println("3. List Items Available");
			System.out.println("Note: In Remove Items if quantity is given negative number book will be removed completely from library");
			switch(Integer.parseInt(reader.readLine().trim())) {
			case 1 : addItemManager(userId, reader);
			break;
			case 2: removeItemsManager(userId, reader);
			break;
			case 3: listItemManager(userId);
			break;
			}
		}
	}
	
	/**
	 * removeItemsManager method is used to remove items from library by manager
	 * @param userId
	 * @param reader2 
	 * @throws NotBoundException
	 * @throws IOException
	 */
	private void removeItemsManager(String userId, BufferedReader reader) throws NotBoundException, IOException {
		System.out.println("Please enter ItemID :");
		String itemId = verifyItemId(reader.readLine().trim(), reader);
		System.out.println("Please enter qunatity");
		int quantity = Integer.parseInt(reader.readLine());
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		insertUserLog(userId, "Remove Book : "+itemId+" : Quantity : "+quantity );
		String response = libraryInterface.removeItem(userId, itemId.toUpperCase(), quantity);
		insertUserLog(userId, "Remove Book : "+itemId+" : Quantity : "+quantity );
		System.out.println("Server Response : "+response);
	}

	/**
	 * listItemManager method is used to get the list of items from library
	 * @param userId
	 * @param reader 
	 * @throws NotBoundException
	 * @throws IOException 
	 * @throws SecurityException 
	 */
	private void listItemManager(String userId) throws NotBoundException, SecurityException, IOException {
		LibraryManagementInterface libraryInterface = connectTheServer(userId);
		insertUserLog(userId, userId+" ListItemsAvailable ");
		String response = libraryInterface.listItemAvailability(userId);
		insertUserLog(userId, userId+"Server Response ListItemsAvailable : \n"+response);
		System.out.println("Server Response : \n"+response);
	}

	/**
	 * addItemManager takes the required data from manager and send to server.
	 * @param userId
	 * @param args 
	 * @param reader2 
	 * @throws NotBoundException
	 * @throws IOException 
	 */
	private void addItemManager(String userId, BufferedReader reader) throws NotBoundException, IOException {
		LibraryManagementInterface libraryServer = connectTheServer(userId);
		if(libraryServer != null) {
			System.out.println("Please enter itemID");
			String itemId = verifyItemId(reader.readLine().trim(), reader);
			System.out.println("Please item Name");
			String itemName = reader.readLine().trim();
			System.out.println("Please enter quantity");
			int quantity = Integer.parseInt(reader.readLine());
			insertUserLog(userId, "Add Item : "+itemId+" : "+ itemName+" : "+quantity);
			String response = libraryServer.addItem(userId, itemId, itemName, quantity);
			insertUserLog(userId, "Server Response Add Item : "+response);
			System.out.println("Server Response : "+response);
		}else {
			System.out.println("Couldn't connect to server...");
		}
	}

	/**
	 * insertUserLog method is to log the data into the respective file
	 * @param managerId
	 * @param message
	 * @throws SecurityException
	 * @throws IOException
	 */
	public void insertUserLog(String userId, String message) throws SecurityException, IOException {
		FileHandler fileHandler = new FileHandler(System.getProperty("user.dir")+LibraryManagementConstants.LOG_FOLDER+userId+".log", true);
		Logger userLogger = Logger.getLogger(userId);
		userLogger.addHandler(fileHandler);
		fileHandler.setFormatter(new SimpleFormatter());
		userLogger.setLevel(Level.INFO);
		userLogger.info(message);
		fileHandler.close();
	}

	/**
	 * verifyItemId method is to check the itemId format
	 * @param itemId
	 * @param reader
	 * @return
	 * @throws IOException
	 */
	private String verifyItemId(String itemId, BufferedReader reader) throws IOException {
		boolean isValidItemName = false;
		while(!isValidItemName) {
			String universityCode = itemId.substring(0, 3).toUpperCase().trim();
			boolean universityCodeVerify = universityCode.equals(LibraryManagementConstants.CONCORDIA_CODE) || 
					universityCode.equals(LibraryManagementConstants.MCGILL_CODE) || 
					universityCode.equals(LibraryManagementConstants.MONTREAL_CODE) && itemId.length() == 7;
			try {
				Integer.parseInt(itemId.substring(4));
				if(universityCodeVerify) {
					isValidItemName = true;
				}else {
					System.out.println("Please provide valid itemId : ");
					itemId = reader.readLine().trim();
				}
			}catch(NumberFormatException exception) {
				System.out.println("Please provide valid itemId : ");
				itemId = reader.readLine().trim();
			}
		}
		return itemId.toUpperCase();
	}

	
	private LibraryManagementInterface connectTheServer(String userId) throws MalformedURLException {
		URL url = null;
		QName qName = null;
		switch (userId.substring(0, 3)) {
		case LibraryManagementConstants.CONCORDIA_CODE:
			url = new URL("http://localhost:8080/libraryserver?wsdl");
			qName = new QName("http://impl.webservice.comp6231.com/", "ConcordiaLibraryImplService");
			break;
		case LibraryManagementConstants.MCGILL_CODE:
			url = new URL("http://localhost:8081/libraryserver?wsdl");
			qName = new QName("http://impl.webservice.comp6231.com/", "McgillLibraryImplService");
			break;
		case LibraryManagementConstants.MONTREAL_CODE:
			url = new URL("http://localhost:8082/libraryserver?wsdl");
			qName = new QName("http://impl.webservice.comp6231.com/", "MontrealLibraryImplService");
			break;
		default:
			break;
		}
		Service service = Service.create(url, qName);
		return service.getPort(LibraryManagementInterface.class);
	}
}
