package com.comp6231.webservice.server;

import javax.xml.ws.Endpoint;

import com.comp6231.webservice.impl.ConcordiaLibraryImpl;

public class ConcordiaLibraryServer {
	public static ConcordiaLibraryImpl concordiaImpl;

	public static void main(String[] args) {
		concordiaImpl = new ConcordiaLibraryImpl();
		
		Runnable concordiaServerStart = () ->{
			startConcordiaServer(concordiaImpl);
		};
		new Thread(concordiaServerStart).start();
	}
	
	private static void startConcordiaServer(ConcordiaLibraryImpl concordiaImpl) {
		Endpoint endpoint = Endpoint.publish("http://localhost:8080/libraryserver", concordiaImpl);
		System.out.println("Concordia Starts");
	}
}
