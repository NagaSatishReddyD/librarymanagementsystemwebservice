package com.comp6231.webservice.server;

import javax.xml.ws.Endpoint;

import com.comp6231.webservice.impl.McgillLibraryImpl;

public class McgillLibraryServer {

	public static void main(String[] args) {
		McgillLibraryImpl mcgillLibraryImpl = new McgillLibraryImpl();
		
		Runnable mcgillServerStart = () ->{
			startMcgillServer(mcgillLibraryImpl);
		};
		new Thread(mcgillServerStart).start();
	}
	
	private static void startMcgillServer(McgillLibraryImpl mcgillLibraryImpl) {
		Endpoint endpoint = Endpoint.publish("http://localhost:8081/libraryserver", mcgillLibraryImpl);
		System.out.println("Mcgill Started");
		
	}
}
