package com.comp6231.webservice.server;

import javax.xml.ws.Endpoint;

import com.comp6231.webservice.impl.MontrealLibraryImpl;

public class MontrealLibraryServer {

	public static void main(String[] args) {
		MontrealLibraryImpl montrealLibraryImpl = new MontrealLibraryImpl();

		Runnable mcgillServerStart = () ->{
			startMontrealServer(montrealLibraryImpl);
		};
		new Thread(mcgillServerStart).start();
	}
	
	private static void startMontrealServer(MontrealLibraryImpl montrealLibraryImpl) {
		Endpoint endpoint = Endpoint.publish("http://localhost:8082/libraryserver", montrealLibraryImpl);
		System.out.println("Montreal Started");
	}
}
