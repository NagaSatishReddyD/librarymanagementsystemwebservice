package com.comp6231.webservice.impl;

import java.io.IOException;
import java.util.Map;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

import com.comp6231.webservice.constants.LibraryManagementConstants;
import com.comp6231.webservice.server.BookData;
import com.comp6231.webservice.serviceinterface.LibraryManagementInterface;

@WebService(endpointInterface="com.comp6231.webservice.serviceinterface.LibraryManagementInterface")
@SOAPBinding(style=SOAPBinding.Style.RPC)
public class ConcordiaLibraryImpl implements LibraryManagementInterface{
	
	private static Map<String, BookData> concordiaBooksData;
	LibraryImplementationHelper implementationHelper = new LibraryImplementationHelper();
	
	
	public ConcordiaLibraryImpl() {
		super();
		try {
			concordiaBooksData = implementationHelper.loadLibraryData(System.getProperty("user.dir")+LibraryManagementConstants.CONCORDIA_INITAL_LOAD_FILE, 
					System.getProperty("user.dir")+LibraryManagementConstants.CONCORDIA_SERVER_LOG_FILE);
		} catch (IOException e) {
			System.out.println("Unknow error while starting the concordia server");
		}
	}

	@Override
	synchronized public String addItem(String managerId, String itemId, String itemName, int quantity) {
		implementationHelper.insertLog(managerId+" : Added "+itemId+" : "+itemName+" : "+ " : "+quantity);
		String response =  implementationHelper.addBookToLibrary(concordiaBooksData, itemId, itemName, quantity);
		implementationHelper.insertLog(managerId+" : Server Response Added "+response);
		return response;
	}
	
	@Override
	synchronized public String listItemAvailability(String managerId) {
		implementationHelper.insertLog(managerId+" : Listed Items");
		String response = implementationHelper.listItemAvailable(concordiaBooksData);
		implementationHelper.insertLog(managerId+" : Listed Items :"+ response);
		return response;
	}
	
	@Override
	synchronized public String borrowItem(String userId, String itemId) {
		String response;
		implementationHelper.insertLog(userId+" : Borrow "+" : "+itemId);
		boolean isFromOtherServers = userId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE) ? false : true;
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response = implementationHelper.borrowItem(concordiaBooksData, userId, itemId, isFromOtherServers);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, null, LibraryManagementConstants.BORROW_ITEM);
		implementationHelper.insertLog(userId+" : Borrow Response"+" : "+itemId+" : "+response);
		return response;
	}
	
	@Override
	synchronized public String addToWaitingList(String userId, String itemId) {
		String response;
		implementationHelper.insertLog(userId+": Added to waiting list for : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response = implementationHelper.addToWaitingList(concordiaBooksData, userId, itemId);
		else
			response = implementationHelper.requestOtherLibraryServers(userId, itemId, null, LibraryManagementConstants.ADD_TO_WAIT);
		implementationHelper.insertLog(userId+": Added to waiting list for : "+itemId+" "+response);
		return response;
	}
	
	@Override
	synchronized public String findItem(String userId, String itemName, boolean fromOtherServer) {
		String response = "";
		implementationHelper.insertLog(userId+" : Find : "+itemName);
		response += implementationHelper.findItem(concordiaBooksData, userId, itemName);
		if(!fromOtherServer) {
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,LibraryManagementConstants.MONTREAL_CODE, LibraryManagementConstants.FIND_ITEM);
			response += implementationHelper.requestOtherLibraryServers(userId,itemName,LibraryManagementConstants.MCGILL_CODE, LibraryManagementConstants.FIND_ITEM);
			response = response.substring(0, response.length());
		}
		implementationHelper.insertLog(userId+" : Find Response: "+itemName +" "+response);
		return response;
	}
	
	@Override
	synchronized public String returnItem(String userId, String itemId) {
		String response;
		implementationHelper.insertLog(userId+" : Returned "+" : "+itemId);
		if(itemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE))
			response =  implementationHelper.returnItem(concordiaBooksData, userId, itemId);
		else
			response =  implementationHelper.requestOtherLibraryServers(userId, itemId, null, LibraryManagementConstants.RETURN_ITEM);
		implementationHelper.insertLog(userId+" : Returned Response"+" : "+response);
		return response;
	}
	
	@Override
	synchronized public String removeItem(String userId, String itemId, int quantity) {
		implementationHelper.insertLog(userId+" : "+"Removed"+" : "+ itemId + " : "+ quantity);
		String response =  implementationHelper.removeItem(concordiaBooksData, itemId, quantity);
		implementationHelper.insertLog(userId+" : "+"Server Response Removed"+" : "+ itemId + " : "+ quantity);
		return response;
	}
	
	@Override
	synchronized public String exchangeItem(String userId, String newItemId, String oldItemId) {
		implementationHelper.insertLog("Exchange Item New Item ID : "+newItemId+" Old Item ID : "+oldItemId);
		String hasTakenBook;
		if(oldItemId.substring(0, 3).equals(LibraryManagementConstants.CONCORDIA_CODE)){
			hasTakenBook = implementationHelper.checkUserTookBook(userId, oldItemId, concordiaBooksData);
		}else {
			hasTakenBook = implementationHelper.requestOtherLibraryServers(userId, oldItemId, null, LibraryManagementConstants.CHECK_USER_TAKEN_BOOK);
		}
		
		String response;
		String hasNewBookInLibrary;
		if(newItemId.substring(0,3).equals(LibraryManagementConstants.CONCORDIA_CODE)) {
			hasNewBookInLibrary = implementationHelper.hasBookInLibraryAndCanBorrow(userId, newItemId,oldItemId, concordiaBooksData, false);
		}else {
			hasNewBookInLibrary = implementationHelper.requestOtherLibraryServers(userId, newItemId+","+oldItemId, null, LibraryManagementConstants.HAS_BOOK_IN_LIBRARY);
		}
		
		if(hasNewBookInLibrary.equals(LibraryManagementConstants.TRUE) && hasTakenBook.equals(LibraryManagementConstants.TRUE)) {
			returnItem(userId, oldItemId);
			if(newItemId.substring(0,3).equals(LibraryManagementConstants.CONCORDIA_CODE)) {
				response = implementationHelper.exchangeItem(userId, newItemId, concordiaBooksData);
			}else {
				response = implementationHelper.requestOtherLibraryServers(userId, newItemId, null, LibraryManagementConstants.EXCHANGE_ITEM);
			}
		}else {
			response = "Can't exchange books";
		}
		implementationHelper.insertLog("Exchange Item New Item ID : "+newItemId+" Old Item ID : "+oldItemId +" Response : "+response);
		return response;
	}
	
	@Override
	synchronized public String hasBookInLibraryAndCanBorrow(String userId, String newItemId, String oldItemId, boolean isFromOtherServer) {
		implementationHelper.insertLog("checkUser Has Taken Book userId :"+userId+" itemId : "+ newItemId );
		String response = implementationHelper.hasBookInLibraryAndCanBorrow(userId, newItemId, oldItemId, concordiaBooksData, isFromOtherServer);
		implementationHelper.insertLog("checkUser Has Taken Book userId :"+userId+" itemId : "+ newItemId +"response "+response);
		return response;
	}

	@Override
	synchronized public String checkUserHasTakenBook(String userId, String itemId) {
		implementationHelper.insertLog("checkUser Has Taken Book userId :"+userId+" itemId : "+ itemId );
		String response = implementationHelper.checkUserTookBook(userId, itemId, concordiaBooksData);
		implementationHelper.insertLog("checkUser Has Taken Book userId :"+userId+" itemId : "+ itemId +"response "+response);
		return response;
	}
	
	@Override
	synchronized public String exchangeItemBorrow(String userId, String itemId) {
		return implementationHelper.exchangeItem(userId, itemId, concordiaBooksData);
	}
	
	synchronized public static Map<String, BookData> getConcordiaBooksData() {
		return concordiaBooksData;
	}
}
