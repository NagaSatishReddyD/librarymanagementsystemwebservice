package com.comp6231.webservice.constants;

public class LibraryManagementConstants {
	
	
	public static final String CONCORDIA_CODE = "CON";
	public static final String MCGILL_CODE = "MCG";
	public static final String MONTREAL_CODE = "MON";
	
	public static final String FIND_ITEM = "FIND_ITEM";
	public static final String BORROW_ITEM = "BORROW_ITEM";
	public static final String RETURN_ITEM = "RETURN_ITEM";
	public static final String EXCHANGE_ITEM = "EXCHANGE_ITEM";
	public static final String ADD_TO_WAIT = "ADD_TO_WAIT";
	public static final String CHECK_USER_TAKEN_BOOK="CHECK_USER_TAKEN_BOOK";
	public static final String HAS_BOOK_IN_LIBRARY="HAS_BOOK_IN_LIBRARY";
	
	public static final String USER_CODE = "U";
	public static final String MANAGER_CODE = "M";
	public static final String CONCORDIA_INITAL_LOAD_FILE = "/resources/ConcordiaLibrary.txt";
	public static final String MCGGILL_INITAL_LOAD_FILE = "/resources/McgillLibrary.txt";
	public static final String MONTREAL_INITAL_LOAD_FILE = "/resources/MontrealLibrary.txt";
	public static final String CONCORDIA_SERVER_LOG_FILE = "/log/concordia_server_log.log";
	public static final String MCGILL_SERVER_LOG_FILE = "/log/mcgill_server_log.log";
	public static final String MONTREAL_SERVER_LOG_FILE = "/log/montreal_server_log.log";
	public static final String LOG_FOLDER = "/log/";
	public static final String WAITING_LIST_MESSAGE="Book not available. Do you want to be added in waiting list?(y/n)";
	public static final String TRUE = "true";
	public static final String FALSE = "false";
	public static final String SOMETHING_WENT_WRONG = "Something went wrong";

}
