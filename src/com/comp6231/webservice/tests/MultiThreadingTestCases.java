package com.comp6231.webservice.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

import com.comp6231.webservice.impl.ConcordiaLibraryImpl;
import com.comp6231.webservice.impl.McgillLibraryImpl;
import com.comp6231.webservice.impl.MontrealLibraryImpl;



public class MultiThreadingTestCases {
	static ConcordiaLibraryImpl concordiaImpl;
	static McgillLibraryImpl mcgillImpl;
	static MontrealLibraryImpl montrealImpl;
	
	@BeforeClass
	public static void beforeEachRun() {
		concordiaImpl=new ConcordiaLibraryImpl();
		mcgillImpl=new McgillLibraryImpl();
		montrealImpl = new MontrealLibraryImpl();
	}


	
	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			mcgillImpl.addItem("MCGM1111", "MCG6231", "Distributed", 1);
			assertEquals(1, mcgillImpl.getMcgillBooksData().get("MCG6231").getQuantity());
		};
		Thread thread1 = new Thread(addItemImplConc);
		Runnable addItemImplMcg = () ->{
			montrealImpl.addItem("MONM1111", "MON6231", "Distributed", 5);
			assertEquals(5, montrealImpl.getMontrealBooksData().get("MON6231").getQuantity());
		};
		Thread thread2 = new Thread(addItemImplMcg);
		Runnable addItemImplMon = () ->{
			concordiaImpl.addItem("CONM1111", "CON6441", "APP", 1);
			assertEquals(1, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
		};
		Thread thread3 = new Thread(addItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
	@Test
	public void listItemThreadTest() {
		Runnable listItemImplConc = () ->{
			mcgillImpl.listItemAvailability("MCGM1111");
		};
		Thread thread1 = new Thread(listItemImplConc);
		Runnable listItemImplMcg = () ->{
			montrealImpl.listItemAvailability("MONM1111");
		};
		Thread thread2 = new Thread(listItemImplMcg);
		Runnable listItemImplMon = () ->{
			concordiaImpl.listItemAvailability("CONM1111");
		};
		Thread thread3 = new Thread(listItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
	@Test
	public void removeItemThreadTest() {
		Runnable removeItemImplConc = () ->{
			mcgillImpl.removeItem("MCGM1111", "MCG6231", 1);
		};
		Thread thread1 = new Thread(removeItemImplConc);
		Runnable removeItemImplMcg = () ->{
			montrealImpl.removeItem("MONM1111", "MON6231", 1);
		};
		Thread thread2 = new Thread(removeItemImplMcg);
		Runnable removeItemImplMon = () ->{
			concordiaImpl.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread3 = new Thread(removeItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
	@Test
	public void borrowItemList() {
		Runnable borrowItemImplConc = () ->{
			concordiaImpl.borrowItem("CONU1111", "CON6231");
		};
		Thread thread1 = new Thread(borrowItemImplConc);
		Runnable borrowItemImplMcg = () ->{
			mcgillImpl.borrowItem("MCGU1111","MCG6231");
		};
		Thread thread2 = new Thread(borrowItemImplMcg);
		Runnable borrowItemImplMon = () ->{
			montrealImpl.borrowItem("MONU1111", "MON6231");
		};
		Thread thread3 = new Thread(borrowItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
	@Test
	public void returnItemThreadTest() {
		Runnable returnItemImplConc = () ->{
			concordiaImpl.returnItem("CONU1111", "CON6231");
		};
		Thread thread1 = new Thread(returnItemImplConc);
		Runnable returnItemImplMcg = () ->{
			mcgillImpl.returnItem("MCGU1111", "MCG6231");
		};
		Thread thread2 = new Thread(returnItemImplConc);
		Runnable returnItemImplMon = () ->{
			montrealImpl.returnItem("MONU1111", "MON6231");
		};
		Thread thread3 = new Thread(returnItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
	
	@Test
	public void findItemThreadTest() {
		Runnable findItemImplConc = () ->{
			concordiaImpl.findItem("CONU1111", "DISTRIBUTED",false);
		};
		Thread thread1 = new Thread(findItemImplConc);
		Runnable findItemImplMcg = () ->{
			mcgillImpl.findItem("MCGU1111","DISTRIBUTED",false);
		};
		Thread thread2 = new Thread(findItemImplMcg);
		Runnable findItemImplMon = () ->{
			montrealImpl.findItem("MONU1111", "DISTRIBUTED",false);
		};
		Thread thread3 = new Thread(findItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}

	@Test
	public void exchangeItemThreadTest() {
		Runnable exchangeItemImplConc = () ->{
			concordiaImpl.exchangeItem("CONU1111", "CON6441",  "CON6231");
			assertTrue(concordiaImpl.getConcordiaBooksData().get("CON6441").getBorrowedList().contains("CONU1111"));
		};
		Thread thread1 = new Thread(exchangeItemImplConc);
		Runnable exchangeItemImplMcg = () ->{
			mcgillImpl.exchangeItem("MCGU1111", "MCG6440",  "MCG6231");
			assertTrue(mcgillImpl.getMcgillBooksData().get("MCG6231").getBorrowedList().contains("MCGU1111"));
		};
		Thread thread2 = new Thread(exchangeItemImplMcg);
		Runnable exchangeItemImplMon = () ->{
			montrealImpl.exchangeItem("MONU1111", "MON6441",  "MON6231");
			assertTrue(montrealImpl.getMontrealBooksData().get("MON6441").getBorrowedList().contains("MONU1111"));
		};
		Thread thread3 = new Thread(exchangeItemImplMon);
		thread1.start();
		thread2.start();
		thread3.start();
	}
}

