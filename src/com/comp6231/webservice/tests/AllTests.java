package com.comp6231.webservice.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ ConcordiaTests.class, McgillTests.class, MontrealTests.class, MultiThreadingTestCases.class })
public class AllTests {

}
