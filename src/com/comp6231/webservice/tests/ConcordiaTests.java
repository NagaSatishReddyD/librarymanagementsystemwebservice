package com.comp6231.webservice.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.comp6231.webservice.impl.ConcordiaLibraryImpl;

public class ConcordiaTests {
	ConcordiaLibraryImpl concordiaImpl;
	
	@Before
	public void beforeEachRun() {
		concordiaImpl=new ConcordiaLibraryImpl();
		concordiaImpl.addItem("CONM1111", "CON6231", "Distributed", 1);
		concordiaImpl.addItem("CONM1111", "CON6231", "Distributed", 5);
		concordiaImpl.addItem("CONM1111", "CON6441", "APP", 1);
	}

	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			concordiaImpl.addItem("CONM1111", "CON6231", "Distributed", 1);
		};
		Thread thread1 = new Thread(addItemImplConc);
		Runnable addItemImplCON = () ->{
			concordiaImpl.addItem("CONM1111", "CON6231", "Distributed", 5);
		};
		Thread thread2 = new Thread(addItemImplCON);
		Runnable addItemImplTwo = () ->{
			concordiaImpl.addItem("CONM1111", "CON6441", "APP", 1);
		};
		Thread thread3 = new Thread(addItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(12, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
	
	@Test
	public void listItemThreadTest() {
		Runnable listItemImplConc = () ->{
			concordiaImpl.listItemAvailability("CONM1111");
		};
		Thread thread1 = new Thread(listItemImplConc);
		Runnable listItemImplCON = () ->{
			concordiaImpl.listItemAvailability("CONM1111");
		};
		Thread thread2 = new Thread(listItemImplCON);
		Runnable listItemImplTwo = () ->{
			concordiaImpl.listItemAvailability("CONM1111");
		};
		Thread thread3 = new Thread(listItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
	
	@Test
	public void removeItemThreadTest() {
		Runnable removeItemImplConc = () ->{
			concordiaImpl.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread1 = new Thread(removeItemImplConc);
		Runnable removeItemImplCON = () ->{
			concordiaImpl.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread2 = new Thread(removeItemImplCON);
		Runnable removeItemImplTwo = () ->{
			concordiaImpl.removeItem("CONM1111", "CON6231", 1);
		};
		Thread thread3 = new Thread(removeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(3, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
	
	@Test
	public void borrowItemList() {
		Runnable borrowItemImplConc = () ->{
			concordiaImpl.borrowItem("CONU1111", "CON6231");
		};
		Thread thread1 = new Thread(borrowItemImplConc);
		Runnable borrowItemImplCON = () ->{
			concordiaImpl.borrowItem("CONU1111","CON6231");
		};
		Thread thread2 = new Thread(borrowItemImplCON);
		Runnable borrowItemImplTwo = () ->{
			concordiaImpl.borrowItem("CONU1111", "CON6231");
		};
		Thread thread3 = new Thread(borrowItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(5, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
	
	@Test
	public void returnItemThreadTest() {
		Runnable returnItemImplConc = () ->{
			concordiaImpl.returnItem("CONM1111", "CON6231");
		};
		Thread thread1 = new Thread(returnItemImplConc);
		Runnable returnItemImplCON = () ->{
			concordiaImpl.returnItem("CON1111", "CON6231");
		};
		Thread thread2 = new Thread(returnItemImplConc);
		Runnable returnItemImpTwo = () ->{
			concordiaImpl.returnItem("CONM1111", "CON6231");
		};
		Thread thread3 = new Thread(returnItemImpTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
	
	@Ignore
	public void findItemThreadTest() {
		Runnable findItemImplConc = () ->{
			concordiaImpl.findItem("CONU1111", "DISTRIBUTED",false);
		};
		Thread thread1 = new Thread(findItemImplConc);
		Runnable findItemImplCON = () ->{
			concordiaImpl.findItem("CONU1111","DISTRIBUTED",false);
		};
		Thread thread2 = new Thread(findItemImplCON);
		Runnable findItemImplTwo = () ->{
			concordiaImpl.findItem("CONU1111", "DISTRIBUTED",false);
		};
		Thread thread3 = new Thread(findItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
	}

	@Ignore
	public void exchangeItemThreadTest() {
		Runnable exchangeItemImplConc = () ->{
			concordiaImpl.exchangeItem("CONM1111", "CON6441",  "CON6231");
		};
		Thread thread1 = new Thread(exchangeItemImplConc);
		Runnable exchangeItemImplCON = () ->{
			concordiaImpl.exchangeItem("CONM1111", "CON6440",  "CON6231");
		};
		Thread thread2 = new Thread(exchangeItemImplCON);
		Runnable exchangeItemImplTwo = () ->{
			concordiaImpl.exchangeItem("CONM1111", "CON6441",  "CON6231");
		};
		Thread thread3 = new Thread(exchangeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, concordiaImpl.getConcordiaBooksData().get("CON6231").getQuantity());
	}
}

