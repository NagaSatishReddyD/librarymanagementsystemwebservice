package com.comp6231.webservice.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.comp6231.webservice.impl.McgillLibraryImpl;

public class McgillTests {
	McgillLibraryImpl mcgillLibraryImpl;
	
	@Before
	public void beforeEachRun() {
		mcgillLibraryImpl=new McgillLibraryImpl();
		mcgillLibraryImpl.addItem("MCGM1111", "MCG6231", "Distributed", 1);
		mcgillLibraryImpl.addItem("MCGM1111", "MCG6231", "Distributed", 5);
		mcgillLibraryImpl.addItem("MCGM1111", "MCG6441", "APP", 1);
	}

	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			mcgillLibraryImpl.addItem("MCGM1111", "MCG6231", "Distributed", 1);
		};
		Thread thread1 = new Thread(addItemImplConc);
		Runnable addItemImplCON = () ->{
			mcgillLibraryImpl.addItem("MCGM1111", "MCG6231", "Distributed", 5);
		};
		Thread thread2 = new Thread(addItemImplCON);
		Runnable addItemImplTwo = () ->{
			mcgillLibraryImpl.addItem("MCGM1111", "MCG6441", "APP", 1);
		};
		Thread thread3 = new Thread(addItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(12, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
	
	@Test
	public void listItemThreadTest() {
		Runnable listItemImplConc = () ->{
			mcgillLibraryImpl.listItemAvailability("MCGM1111");
		};
		Thread thread1 = new Thread(listItemImplConc);
		Runnable listItemImplCON = () ->{
			mcgillLibraryImpl.listItemAvailability("MCGM1111");
		};
		Thread thread2 = new Thread(listItemImplCON);
		Runnable listItemImplTwo = () ->{
			mcgillLibraryImpl.listItemAvailability("MCGM1111");
		};
		Thread thread3 = new Thread(listItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
	
	@Test
	public void removeItemThreadTest() {
		Runnable removeItemImplConc = () ->{
			mcgillLibraryImpl.removeItem("MCGM1111", "MCG6231", 1);
		};
		Thread thread1 = new Thread(removeItemImplConc);
		Runnable removeItemImplCON = () ->{
			mcgillLibraryImpl.removeItem("MCGM1111", "MCG6231", 1);
		};
		Thread thread2 = new Thread(removeItemImplCON);
		Runnable removeItemImplTwo = () ->{
			mcgillLibraryImpl.removeItem("MCGM1111", "MCG6231", 1);
		};
		Thread thread3 = new Thread(removeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(3, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
	
	@Test
	public void borrowItemList() {
		Runnable borrowItemImplConc = () ->{
			mcgillLibraryImpl.borrowItem("MCGU1111", "MCG6231");
		};
		Thread thread1 = new Thread(borrowItemImplConc);
		Runnable borrowItemImplCON = () ->{
			mcgillLibraryImpl.borrowItem("MCGU1111","MCG6231");
		};
		Thread thread2 = new Thread(borrowItemImplCON);
		Runnable borrowItemImplTwo = () ->{
			mcgillLibraryImpl.borrowItem("MCGU1111", "MCG6231");
		};
		Thread thread3 = new Thread(borrowItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(5, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
	
	@Test
	public void returnItemThreadTest() {
		Runnable returnItemImplConc = () ->{
			mcgillLibraryImpl.returnItem("MCGM1111", "MCG6231");
		};
		Thread thread1 = new Thread(returnItemImplConc);
		Runnable returnItemImplCON = () ->{
			mcgillLibraryImpl.returnItem("MCG1111", "MCG6231");
		};
		Thread thread2 = new Thread(returnItemImplConc);
		Runnable returnItemImpTwo = () ->{
			mcgillLibraryImpl.returnItem("MCGM1111", "MCG6231");
		};
		Thread thread3 = new Thread(returnItemImpTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
	
	@Ignore
	public void findItemThreadTest() {
		Runnable findItemImplConc = () ->{
			mcgillLibraryImpl.findItem("MCGU1111", "DISTRIBUTED",false);
		};
		Thread thread1 = new Thread(findItemImplConc);
		Runnable findItemImplCON = () ->{
			mcgillLibraryImpl.findItem("MCGU1111","DISTRIBUTED",false);
		};
		Thread thread2 = new Thread(findItemImplCON);
		Runnable findItemImplTwo = () ->{
			mcgillLibraryImpl.findItem("MCGU1111", "DISTRIBUTED",false);
		};
		Thread thread3 = new Thread(findItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
	}

	@Ignore
	public void exchangeItemThreadTest() {
		Runnable exchangeItemImplConc = () ->{
			mcgillLibraryImpl.exchangeItem("MCGM1111", "MCG6441",  "MCG6231");
		};
		Thread thread1 = new Thread(exchangeItemImplConc);
		Runnable exchangeItemImplCON = () ->{
			mcgillLibraryImpl.exchangeItem("MCGM1111", "MCG6440",  "MCG6231");
		};
		Thread thread2 = new Thread(exchangeItemImplCON);
		Runnable exchangeItemImplTwo = () ->{
			mcgillLibraryImpl.exchangeItem("MCGM1111", "MCG6441",  "MCG6231");
		};
		Thread thread3 = new Thread(exchangeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, mcgillLibraryImpl.getMcgillBooksData().get("MCG6231").getQuantity());
	}
}

