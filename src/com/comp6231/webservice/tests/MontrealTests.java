package com.comp6231.webservice.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.comp6231.webservice.impl.MontrealLibraryImpl;

public class MontrealTests {
	MontrealLibraryImpl montrealLibraryImpl;
	
	@Before
	public void beforeEachRun() {
		montrealLibraryImpl=new MontrealLibraryImpl();
		montrealLibraryImpl.addItem("MONM1111", "MON6231", "Distributed", 1);
		montrealLibraryImpl.addItem("MONM1111", "MON6231", "Distributed", 5);
		montrealLibraryImpl.addItem("MONM1111", "MON6441", "APP", 1);
	}

	@Test
	public void addItemThreadTest() {
		Runnable addItemImplConc = () ->{
			montrealLibraryImpl.addItem("MONM1111", "MON6231", "Distributed", 1);
		};
		Thread thread1 = new Thread(addItemImplConc);
		Runnable addItemImplCON = () ->{
			montrealLibraryImpl.addItem("MONM1111", "MON6231", "Distributed", 5);
		};
		Thread thread2 = new Thread(addItemImplCON);
		Runnable addItemImplTwo = () ->{
			montrealLibraryImpl.addItem("MONM1111", "MON6441", "APP", 1);
		};
		Thread thread3 = new Thread(addItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(12, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
	
	@Test
	public void listItemThreadTest() {
		Runnable listItemImplConc = () ->{
			montrealLibraryImpl.listItemAvailability("MONM1111");
		};
		Thread thread1 = new Thread(listItemImplConc);
		Runnable listItemImplCON = () ->{
			montrealLibraryImpl.listItemAvailability("MONM1111");
		};
		Thread thread2 = new Thread(listItemImplCON);
		Runnable listItemImplTwo = () ->{
			montrealLibraryImpl.listItemAvailability("MONM1111");
		};
		Thread thread3 = new Thread(listItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
	
	@Test
	public void removeItemThreadTest() {
		Runnable removeItemImplConc = () ->{
			montrealLibraryImpl.removeItem("MONM1111", "MON6231", 1);
		};
		Thread thread1 = new Thread(removeItemImplConc);
		Runnable removeItemImplCON = () ->{
			montrealLibraryImpl.removeItem("MONM1111", "MON6231", 1);
		};
		Thread thread2 = new Thread(removeItemImplCON);
		Runnable removeItemImplTwo = () ->{
			montrealLibraryImpl.removeItem("MONM1111", "MON6231", 1);
		};
		Thread thread3 = new Thread(removeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(3, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
	
	@Test
	public void borrowItemList() {
		Runnable borrowItemImplConc = () ->{
			montrealLibraryImpl.borrowItem("MONU1111", "MON6231");
		};
		Thread thread1 = new Thread(borrowItemImplConc);
		Runnable borrowItemImplCON = () ->{
			montrealLibraryImpl.borrowItem("MONU1111","MON6231");
		};
		Thread thread2 = new Thread(borrowItemImplCON);
		Runnable borrowItemImplTwo = () ->{
			montrealLibraryImpl.borrowItem("MONU1111", "MON6231");
		};
		Thread thread3 = new Thread(borrowItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(5, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
	
	@Test
	public void returnItemThreadTest() {
		Runnable returnItemImplConc = () ->{
			montrealLibraryImpl.returnItem("MONM1111", "MON6231");
		};
		Thread thread1 = new Thread(returnItemImplConc);
		Runnable returnItemImplCON = () ->{
			montrealLibraryImpl.returnItem("MON1111", "MON6231");
		};
		Thread thread2 = new Thread(returnItemImplConc);
		Runnable returnItemImpTwo = () ->{
			montrealLibraryImpl.returnItem("MONM1111", "MON6231");
		};
		Thread thread3 = new Thread(returnItemImpTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
	
	@Ignore
	public void findItemThreadTest() {
		Runnable findItemImplConc = () ->{
			montrealLibraryImpl.findItem("MONU1111", "DISTRIBUTED",false);
		};
		Thread thread1 = new Thread(findItemImplConc);
		Runnable findItemImplCON = () ->{
			montrealLibraryImpl.findItem("MONU1111","DISTRIBUTED",false);
		};
		Thread thread2 = new Thread(findItemImplCON);
		Runnable findItemImplTwo = () ->{
			montrealLibraryImpl.findItem("MONU1111", "DISTRIBUTED",false);
		};
		Thread thread3 = new Thread(findItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
	}

	@Ignore
	public void exchangeItemThreadTest() {
		Runnable exchangeItemImplConc = () ->{
			montrealLibraryImpl.exchangeItem("MONM1111", "MON6441",  "MON6231");
		};
		Thread thread1 = new Thread(exchangeItemImplConc);
		Runnable exchangeItemImplCON = () ->{
			montrealLibraryImpl.exchangeItem("MONM1111", "MON6440",  "MON6231");
		};
		Thread thread2 = new Thread(exchangeItemImplCON);
		Runnable exchangeItemImplTwo = () ->{
			montrealLibraryImpl.exchangeItem("MONM1111", "MON6441",  "MON6231");
		};
		Thread thread3 = new Thread(exchangeItemImplTwo);
		thread1.start();
		thread2.start();
		thread3.start();
		while(thread1.isAlive() || thread2.isAlive() || thread3.isAlive());
		assertEquals(6, montrealLibraryImpl.getMontrealBooksData().get("MON6231").getQuantity());
	}
}

